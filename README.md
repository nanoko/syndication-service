# Syndication Service
The Syndication Service specifies a [h-ubu](http://nanoko-project.github.com/h-ubu/snapshot/) service to consume feeds
(RSS and ATOM).

This projects contains:

 * The contracts (i.e. interfaces) :
 * A implementation based on JFeed :

You can download the different files from the download page.

# License
This project is licensed under the Apache License 2.0. The project is founded by [Arrow-Group](http://arrow-group.eu).
